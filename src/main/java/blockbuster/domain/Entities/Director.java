package blockbuster.domain.Entities;

import javafx.beans.property.SimpleStringProperty;

public class Director {

    SimpleStringProperty id;
    SimpleStringProperty director;

    public Director(String id, String director) {
        this.id = new SimpleStringProperty(id);
        this.director = new SimpleStringProperty(director);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getdirector() {
        return director.get();
    }

    public SimpleStringProperty directorProperty() {
        return director;
    }

    public void setdirector(String director) {
        this.director.set(director);
    }
}
