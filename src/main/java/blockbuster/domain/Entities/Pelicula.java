package blockbuster.domain.Entities;

import blockbuster.domain.contracts.Ientity;
import javafx.beans.property.SimpleStringProperty;

public class Pelicula implements Ientity {

    SimpleStringProperty id;
    SimpleStringProperty titulo;
    SimpleStringProperty genero;
    SimpleStringProperty ano;
    SimpleStringProperty director;
    SimpleStringProperty duracion;
    SimpleStringProperty sinopsis;

    public Pelicula(String id, String titulo, String genero, String ano, String director, String duracion, String sinopsis) {
        this.id = new SimpleStringProperty(id);
        this.titulo = new SimpleStringProperty(titulo);
        this.genero = new SimpleStringProperty(genero);
        this.ano = new SimpleStringProperty(ano);
        this.director = new SimpleStringProperty(director);
        this.duracion = new SimpleStringProperty(duracion);
        this.sinopsis = new SimpleStringProperty(sinopsis);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getTitulo() {
        return titulo.get();
    }

    public SimpleStringProperty tituloProperty() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo.set(titulo);
    }

    public String getGenero() {
        return genero.get();
    }

    public SimpleStringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    public String getAno() {
        return ano.get();
    }

    public SimpleStringProperty anoProperty() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano.set(ano);
    }

    public String getDirector() {
        return director.get();
    }

    public SimpleStringProperty directorProperty() {
        return director;
    }

    public void setDirector(String director) {
        this.director.set(director);
    }

    public String getDuracion() {
        return duracion.get();
    }

    public SimpleStringProperty duracionProperty() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion.set(duracion);
    }

    public String getSinopsis() {
        return sinopsis.get();
    }

    public SimpleStringProperty sinopsisProperty() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis.set(sinopsis);
    }
}
