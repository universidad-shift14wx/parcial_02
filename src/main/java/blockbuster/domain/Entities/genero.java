package blockbuster.domain.Entities;

import blockbuster.domain.contracts.Ientity;
import javafx.beans.property.SimpleStringProperty;

public class genero implements Ientity {

    SimpleStringProperty id;
    SimpleStringProperty titulo;

    public genero(String id, String titulo) {
        this.id = new SimpleStringProperty(id);
        this.titulo = new SimpleStringProperty(titulo);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getTitulo() {
        return titulo.get();
    }

    public SimpleStringProperty tituloProperty() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo.set(titulo);
    }
}
