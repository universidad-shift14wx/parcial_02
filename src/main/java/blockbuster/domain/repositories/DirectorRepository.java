package blockbuster.domain.repositories;

import blockbuster.domain.Entities.Director;
import blockbuster.domain.Entities.genero;
import blockbuster.domain.contracts.Ientity;
import blockbuster.domain.contracts.Irepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DirectorRepository implements Irepository {

    private ObservableList<Director> directoresList = FXCollections.observableArrayList();

    public DirectorRepository(){
        this.directoresList.add(new Director(
                "1",
                "Steven Spielberg"
        ));
        this.directoresList.add(new Director(
                "2",
                "Peter Jackson"
        ));
        this.directoresList.add(new Director(
                "3",
                "Martin Scorsese "
        ));
        this.directoresList.add(new Director(
                "4",
                "Christopher Nolan "
        ));
        this.directoresList.add(new Director(
                "5",
                "Steven Soderbergh "
        ));
        this.directoresList.add(new Director(
                "6",
                "Ridley Scott"
        ));
        this.directoresList.add(new Director(
                "7",
                "Quentin Tarantino"
        ));
        this.directoresList.add(new Director(
                "8",
                "Michael Mann"
        ));
        this.directoresList.add(new Director(
                "9",
                "James Cameron"
        ));
        this.directoresList.add(new Director(
                "10",
                "Joel and Ethan Coen"
        ));
    }

    @Override
    public void save(Ientity director) {
        this.directoresList.add((Director) director);
    }

    @Override
    public ObservableList<Director> all() {
        return directoresList;
    }

    public ObservableList<String> stringDirectores(){
        ObservableList<String> directoresString = FXCollections.observableArrayList();

        directoresList.forEach(ev->{
            directoresString.add(ev.getdirector());
        });
        return directoresString;
    }
}
