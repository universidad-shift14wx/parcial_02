package blockbuster.domain.repositories;

import blockbuster.domain.Entities.genero;
import blockbuster.domain.contracts.Ientity;
import blockbuster.domain.contracts.Irepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collection;

public class GenerosRepository implements Irepository {


    private ObservableList<genero> generosList = FXCollections.observableArrayList();

    public GenerosRepository() {

        this.generosList.add(new genero(
                "1",
                "Accion"
        ));

        this.generosList.add(new genero(
                "2",
                "Drama"
        ));
        this.generosList.add(new genero(
                "3",
                "Romance"
        ));
        this.generosList.add(new genero(
                "4",
                "Comedia"
        ));
        this.generosList.add(new genero(
                "5",
                "Fantasia"
        ));


    }


    @Override
    public void save(Ientity cateoria) {
        this.generosList.add((genero) cateoria);
    }

    @Override
    public ObservableList<genero> all() {
        return generosList;
    }

    public ObservableList<String> stringCategories(){
        ObservableList<String> categoriesNames = FXCollections.observableArrayList();

        generosList.forEach(ev->{
            categoriesNames.add(ev.getTitulo());
        });
        return categoriesNames;
    }
}
