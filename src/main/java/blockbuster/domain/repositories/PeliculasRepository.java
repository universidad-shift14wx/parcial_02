package blockbuster.domain.repositories;

import blockbuster.domain.Entities.Pelicula;
import blockbuster.domain.contracts.Ientity;
import blockbuster.domain.contracts.Irepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PeliculasRepository implements Irepository {

    private Pelicula pelicula;

    private ObservableList<Pelicula> listPeliculas = FXCollections.observableArrayList();

    public PeliculasRepository(){
        this.listPeliculas.add(new Pelicula(
                "1",
                "Robocot",
                "Acción/Ciencia ficción",
                "1987",
                "Paul Verhoeven",
                "1h 43m",
                "Un grupo de científicos utiliza los restos destrozados de un policía muerto para crear al mejor luchador contra el crimen: un robot indestructible. El experimento parece un éxito, pero el policía, a pesar de estar muerto, conserva la memoria y decide vengarse de sus asesinos."
        ));

        this.listPeliculas.add(new Pelicula(
                "2",
                "Isla de perros",
                "Animación/Comedia",
                "2018",
                "Paul Verhoeven",
                "1h 45m",
                "Un grupo de científicos utiliza los restos destrozados de un policía muerto para crear al mejor luchador contra el crimen: un robot indestructible. El experimento parece un éxito, pero el policía, a pesar de estar muerto, conserva la memoria y decide vengarse de sus asesinos."
        ));
    }

    @Override
    public void save(Ientity pelicula) {

        this.listPeliculas.add((Pelicula) pelicula);

    }

    public void Delete(int index){
        this.listPeliculas.remove(index);
    }

    @Override
    public ObservableList<Pelicula> all() {
        return this.listPeliculas;
    }
}
