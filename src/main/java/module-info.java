module org.labtwo {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.jfoenix;
    requires AnimateFX;

    opens org.labtwo to javafx.fxml;
    exports org.labtwo;
}