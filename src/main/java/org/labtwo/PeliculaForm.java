package org.labtwo;

import animatefx.animation.FadeInUp;
import animatefx.animation.FadeOutDown;
import animatefx.animation.Shake;
import blockbuster.domain.Entities.Pelicula;
import blockbuster.domain.repositories.DirectorRepository;
import blockbuster.domain.repositories.GenerosRepository;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class PeliculaForm implements Initializable {

    GenerosRepository generosRepository;
    DirectorRepository directorRepository;

    public PeliculaForm() {
        generosRepository = new GenerosRepository();
        directorRepository = new DirectorRepository();
    }

    @FXML
    public TextArea sinopsis;
    @FXML
    public TextField titulo;
    @FXML
    public TextField duracion;
    @FXML
    public ComboBox<String> genero;
    @FXML
    public ComboBox<String> director;
    @FXML
    public Spinner<Integer> ano;
    @FXML
    public Label errorLabel;
    @FXML
    public AnchorPane anchorDone;

    public boolean error = true;

    public boolean visible = false;

    @FXML
    public JFXButton btnAdd;

    @FXML
    public JFXButton btnSuccessDelete;

    @FXML
    public JFXButton btnDanger;

    @FXML
    public void hideError(){
        new FadeOutDown(this.errorLabel).play();
        this.visible = false;
    }
    @FXML
    public void showError(){
        if(this.visible){
            new Shake(this.errorLabel).play();
        }else{
            new FadeInUp(this.errorLabel).play();
            this.visible = true;
        }
    }

    @FXML
    public void Validate(){
        try {
            if(!this.titulo.getText().equals("") && !this.director.getValue().equals("") && !this.sinopsis.getText().equals("") && !this.duracion.getText().equals("") && !this.genero.getValue().equals("")){
                this.error=false;
                this.hideError();
            }else{
                this.error = true;
                this.showError();

            }
        }catch (Exception e){
            this.showError();
        }

    }

    public void setPeliculaEditar(Pelicula pelicula){
        this.titulo.setText(pelicula.getTitulo().toString());
       this.setSpinnerAno(Integer.parseInt(pelicula.getAno()));
       this.director.setValue(pelicula.getDirector().toString());
       this.duracion.setText(pelicula.getSinopsis());
       this.genero.setValue(pelicula.getGenero());
       this.sinopsis.setText(pelicula.getSinopsis());
       new FadeInUp(this.btnDanger).play();
    }

    public void setSpinnerAno(Integer Valor){
        int setValor = Valor>-1 ? Valor:1990;
        this.ano.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1000,2021,setValor));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new FadeOutDown(this.anchorDone).play();
        this.anchorDone.toBack();
        new FadeOutDown(this.btnDanger).play();
        this.hideError();
        this.genero.setItems(this.generosRepository.stringCategories());
        this.director.setItems(this.directorRepository.stringDirectores());

        this.genero.setValue(this.generosRepository.stringCategories().get(0));
        this.director.setValue(this.directorRepository.stringDirectores().get(0));
        this.setSpinnerAno(-1);
    }

    public void confirmDelete(){
        ButtonType si = new ButtonType("SI");
        ButtonType no = new ButtonType("NO");
        Alert confirmDelete = new Alert(Alert.AlertType.NONE,"¿Desea Eliminar Este registro?",si,no);
        confirmDelete.setTitle("¿Desea eliminar este registro?");
        confirmDelete.showAndWait().ifPresent(response -> {
            if (response == si){
                this.anchorDone.toFront();
                new FadeInUp(this.anchorDone).play();
            }
        });
    }
}
