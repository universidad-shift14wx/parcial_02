package org.labtwo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeInRight;
import animatefx.animation.FadeOutLeft;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;

public class PrimaryController implements Initializable {

    @FXML
    private AnchorPane primary;
    @FXML
    private AnchorPane navbar;

    public boolean isVisible = false;
    @FXML
    public void showNavbar(){
        if(this.isVisible){
            this.closeNavbar();
        }else{
            new FadeInLeft(this.navbar).play();
        }
        this.isVisible = !this.isVisible;
    }

    @FXML
    public void closeNavbar(){
        new FadeOutLeft(this.navbar).play();
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // hide navbar
        this.closeNavbar();
        this.isVisible = false;
        Node node;
        try {
            node = (Node)FXMLLoader.load(getClass().getResource("/org/labtwo/includes/navbar.fxml"));
            this.navbar.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
