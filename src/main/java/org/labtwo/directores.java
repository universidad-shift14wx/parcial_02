package org.labtwo;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeOutLeft;
import blockbuster.domain.Entities.Director;
import blockbuster.domain.Entities.Pelicula;
import blockbuster.domain.repositories.DirectorRepository;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.security.cert.PolicyNode;
import java.util.ResourceBundle;

public class directores implements Initializable {

    DirectorRepository directorRepository;

    @FXML
    private TableColumn<Director,String> tcId = new TableColumn<Director,String>();
    @FXML
    private TableColumn<Director,String> tcTitulo = new TableColumn<Director,String>() ;
    private boolean isVisible;
    @FXML
    private AnchorPane navbar;


    @FXML
    public void showNavbar(){
        if(this.isVisible){
            this.closeNavbar();
        }else{
            new FadeInLeft(this.navbar).play();
        }
        this.isVisible = !this.isVisible;
    }

    @FXML
    public void closeNavbar(){
        new FadeOutLeft(this.navbar).play();
    }



    @FXML
    private TableView<Director> tableU;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.directorRepository = new DirectorRepository();
    this.setupTable();

        // hide navbar
        this.closeNavbar();
        this.isVisible = false;
        Node node;
        try {
            node = (Node) FXMLLoader.load(getClass().getResource("/org/labtwo/includes/navbar.fxml"));
            this.navbar.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setupTable(){
        this.tcId.setCellValueFactory(name->name.getValue().idProperty());
        this.tcTitulo.setCellValueFactory(code->code.getValue().directorProperty());

        this.tableU.setItems(this.directorRepository.all());
    }
}
