package org.labtwo;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeOutLeft;
import blockbuster.domain.Entities.genero;
import blockbuster.domain.repositories.GenerosRepository;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class generos implements Initializable {

    GenerosRepository generosRepository;

    @FXML
    private TableColumn<genero,String> tcId = new TableColumn<genero,String>();
    @FXML
    private TableColumn<genero,String> tcTitulo = new TableColumn<genero,String>() ;
    private boolean isVisible;
    @FXML
    private AnchorPane navbar;


    @FXML
    public void showNavbar(){
        if(this.isVisible){
            this.closeNavbar();
        }else{
            new FadeInLeft(this.navbar).play();
        }
        this.isVisible = !this.isVisible;
    }

    @FXML
    public void closeNavbar(){
        new FadeOutLeft(this.navbar).play();
    }



    @FXML
    private TableView<genero> tableU;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.generosRepository = new GenerosRepository();
    this.setupTable();

        // hide navbar
        this.closeNavbar();
        this.isVisible = false;
        Node node;
        try {
            node = (Node) FXMLLoader.load(getClass().getResource("/org/labtwo/includes/navbar.fxml"));
            this.navbar.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setupTable(){
        this.tcId.setCellValueFactory(name->name.getValue().idProperty());
        this.tcTitulo.setCellValueFactory(code->code.getValue().tituloProperty());

        this.tableU.setItems(this.generosRepository.all());
    }
}
