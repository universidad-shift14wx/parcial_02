package org.labtwo;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import org.labtwo.App;

import java.io.IOException;

public class navbarController {

    @FXML
    public JFXButton btndashboard;
    @FXML
    public JFXButton btnpeliculas;
    @FXML
    public JFXButton btndirectores;
    @FXML
    public JFXButton btngeneros;

    @FXML
    private void switchToMovies() throws IOException {
        App.setRoot("peliculas");
    }

    @FXML
    private void switchToHome() throws IOException {
        App.setRoot("primary");
    }

    @FXML
    private void switchToGenero() throws IOException {
        App.setRoot("generos");
    }

    @FXML
    private void switchToDirectores() throws IOException {
        App.setRoot("directores");
    }

}
