package org.labtwo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeInUp;
import animatefx.animation.FadeOutDown;
import animatefx.animation.FadeOutLeft;
import blockbuster.domain.Entities.Pelicula;
import blockbuster.domain.repositories.PeliculasRepository;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class peliculasController implements Initializable {


    @FXML
    private AnchorPane navbar;
    PeliculasRepository productoUseCase;
    JFXDialogLayout dialogLayout;
    JFXDialog dialog;
    public peliculasController(){
        productoUseCase = new PeliculasRepository();
    }

    public boolean isVisible = false;


    enum estadoFormulario {
        AGREGAR,
        EDITAR
    }

    @FXML
    private TableColumn<Pelicula,String> tcId = new TableColumn<Pelicula,String>();
    @FXML
    private TableColumn<Pelicula,String> tcTitulo = new TableColumn<Pelicula,String>() ;
    @FXML
    private TableColumn<Pelicula,String> tcDirector= new TableColumn<Pelicula,String>();
    @FXML
    private TableColumn<Pelicula,String> tcAno= new TableColumn<Pelicula,String>();
    @FXML
    private TableColumn<Pelicula,String> tcDuracion= new TableColumn<Pelicula,String>();
    @FXML
    private TableColumn<Pelicula,String> tcGenero= new TableColumn<Pelicula,String>();
    @FXML
    private TableColumn<Pelicula,String> tcSinopsis= new TableColumn<Pelicula,String>();

    @FXML
    private TableView<Pelicula> tablePeliculas;


    @FXML
    private JFXButton addBtn;

    @FXML
    private StackPane stackPaneForm;
    @FXML
    private AnchorPane stackAnchor;


    private Enum estado = estadoFormulario.AGREGAR;
    private Integer indexTable = -1;
    private Pelicula peliculaEditar;

    @FXML
    public void showForm() throws IOException {
        System.out.println("show form");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/labtwo/includes/peliculaForm.fxml"));
        dialogLayout = new JFXDialogLayout();
        Parent parent =(Parent) loader.load();
        dialogLayout.setBody(parent);
        new FadeInUp(this.stackAnchor).play();
        dialog = new JFXDialog(stackPaneForm, dialogLayout, JFXDialog.DialogTransition.BOTTOM);
        // catch controller
        PeliculaForm controlador = loader.getController();
        if(indexTable>-1){
            controlador.setPeliculaEditar(this.peliculaEditar);
        }

        controlador.btnSuccessDelete.setOnMouseClicked(Event->{
            this.productoUseCase.Delete(this.indexTable);
            this.hideAnchor();
            Alert conf = new Alert(Alert.AlertType.INFORMATION,"Se elimino la pelicula");
            conf.setTitle("Eliminado");
            conf.show();
        });

        controlador.btnAdd.setOnMouseClicked(Event->{
            if(!controlador.error){
                this.hideAnchor();
                int lastId= Integer.parseInt(this.productoUseCase.all().get(this.productoUseCase.all().size()-1).getId());
                this.productoUseCase.save(new Pelicula(
                        String.valueOf((lastId+1)),
                        controlador.titulo.getText(),
                        controlador.genero.getValue().toString(),
                        controlador.ano.getValue().toString(),
                        controlador.director.getValue().toString(),
                        controlador.duracion.getText(),
                        controlador.sinopsis.getText()
                ));

                Alert conf = new Alert(Alert.AlertType.INFORMATION,"Se Agrego la pelicula");
                conf.setTitle("Agregada");
                conf.show();
            }
        });
        dialog.show();
    }

    public void setupTable(){
        this.tcId.setCellValueFactory(name->name.getValue().idProperty());
        this.tcTitulo.setCellValueFactory(code->code.getValue().tituloProperty());
        this.tcAno.setCellValueFactory(category->category.getValue().anoProperty());
        this.tcDirector.setCellValueFactory(precio->precio.getValue().directorProperty());
        this.tcDuracion.setCellValueFactory(description->description.getValue().duracionProperty());
        this.tcGenero.setCellValueFactory(description->description.getValue().generoProperty());
        this.tcSinopsis.setCellValueFactory(description->description.getValue().sinopsisProperty());

        this.tablePeliculas.setItems(this.productoUseCase.all());
    }

    @FXML
    public void hideAnchor(){
        new FadeOutDown(this.stackAnchor).play();
        this.indexTable = -1;
    }

    public void setTableListener(){
        try {
            indexTable = this.tablePeliculas.getSelectionModel().getSelectedIndex();
            if(indexTable>-1){
                this.peliculaEditar = this.tablePeliculas.getSelectionModel().getSelectedItem();
                estado = estadoFormulario.EDITAR;
                this.showForm();
            }
        }catch (Exception ignored){

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        new FadeOutDown(this.stackAnchor).play();
        new FadeOutLeft(this.navbar).play();
        this.setupTable();
        // hide navbar
        this.closeNavbar();
        this.isVisible = false;
        Node node;
        try {
            node = (Node)FXMLLoader.load(getClass().getResource("/org/labtwo/includes/navbar.fxml"));
            this.navbar.getChildren().setAll(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    public void showNavbar(){
        if(this.isVisible){
            this.closeNavbar();
        }else{
            new FadeInLeft(this.navbar).play();
        }
        this.isVisible = !this.isVisible;
    }

    @FXML
    public void closeNavbar(){
        new FadeOutLeft(this.navbar).play();
    }


}